//
//  CDPlanetExtension.swift
//  JPM_Challenge
//
//  Created by Gill on 31/05/2023.
//

import Foundation
import CoreData

extension CD_Planet {
    func convertToPlant() -> Results {
        return Results(name: self.name, rotationPeriod: self.rotation_period, orbitalPeriod: self.orbital_period, diameter: self.diameter, climate: self.climate, gravity: self.gravity, terrain: self.terrain, surfaceWater: self.surface_water, population: self.population, residents: self.residents as? [String], films: self.films as? [String], created: self.created, edited: self.edited, url: self.url)
    }
}



@objc(CDAnimal)
public class CDAnimal: NSManagedObject {

}
