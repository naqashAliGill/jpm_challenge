//
//  PlanetsCoreDataRepo.swift
//  JPM_Challenge
//
//  Created by Gill on 31/05/2023.
//

import Foundation
import CoreData

struct PlanetsCoreDataRepo : PlanetCoreDataRepository {

     
   func insertPlanetRecords(records: Array<Results>) -> Bool {

        debugPrint("PlanetDataRepository: Insert record operation is starting")

        PersistentStorage.shared.persistentContainer.performBackgroundTask { privateManagedContext in
            //insert code
            records.forEach { plantRecord in
                let cdPlanet = CD_Planet(context: privateManagedContext)
               
                cdPlanet.name = plantRecord.name
                cdPlanet.diameter = plantRecord.diameter
                cdPlanet.climate = plantRecord.climate
                cdPlanet.created = plantRecord.created
                cdPlanet.edited = plantRecord.edited
                cdPlanet.gravity = plantRecord.gravity
                cdPlanet.films = plantRecord.films as NSObject?
                cdPlanet.url = plantRecord.url
                cdPlanet.orbital_period = plantRecord.orbital_period
                cdPlanet.terrain = plantRecord.terrain
                cdPlanet.population = plantRecord.population
                cdPlanet.surface_water = plantRecord.surface_water
                cdPlanet.rotation_period = plantRecord.rotation_period
                cdPlanet.residents = plantRecord.residents as NSObject?
            }

            if(privateManagedContext.hasChanges){
                try? privateManagedContext.save()
                debugPrint("PlanetDataRepository: Insert record operation is completed")
            }
        }

        return true
    }


    func getPlanetRecords(completionHandler: @escaping (Array<Results>?) -> Void) {

        PersistentStorage.shared.printDocumentDirectoryPath()

        let result = PersistentStorage.shared.fetchManagedObject(managedObject: CD_Planet.self)
            var planets : Array<Results> = []
            result?.forEach({ (cdPlanet) in
                planets.append(cdPlanet.convertToPlant())
            })

       
            completionHandler(planets)

    }
}
