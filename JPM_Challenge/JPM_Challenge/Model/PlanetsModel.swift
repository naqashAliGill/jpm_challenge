//
//  PlanetsModel.swift
//  JPM_Challenge
//
//  Created by Gill on 30/05/2023.
//

import Foundation
class PlanetsModelList : Codable {
    var count : Int?
    var next : String?
    var previous : String?
    var results : [Results]?
    
    enum CodingKeys: String, CodingKey {
        
        case count = "count"
        case next = "next"
        case previous = "previous"
        case results = "results"
    }
}
typealias PlanetsModel = [String: PlanetsModelList]
