//
//  Results.swift
//  JPM_Challenge
//
//  Created by Gill on 30/05/2023.
//

import Foundation

class Results : Codable {
    var name : String?
    var rotation_period : String?
    var orbital_period : String?
    var diameter : String?
    var climate : String?
    var gravity : String?
    var terrain : String?
    var surface_water : String?
    var population : String?
    var residents : [String]?
    var films : [String]?
    var created : String?
    var edited : String?
    var url : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case rotation_period = "rotation_period"
        case orbital_period = "orbital_period"
        case diameter = "diameter"
        case climate = "climate"
        case gravity = "gravity"
        case terrain = "terrain"
        case surface_water = "surface_water"
        case population = "population"
        case residents = "residents"
        case films = "films"
        case created = "created"
        case edited = "edited"
        case url = "url"
    }
      init(name: String?, rotationPeriod: String?, orbitalPeriod: String?, diameter: String?, climate: String?, gravity: String?, terrain: String?, surfaceWater: String?, population: String?, residents: [String]?, films: [String]?, created: String?, edited: String?, url: String?) {
          self.name = name
          self.rotation_period = rotationPeriod
          self.orbital_period = orbitalPeriod
          self.diameter = diameter
          self.climate = climate
          self.gravity = gravity
          self.terrain = terrain
          self.surface_water = surfaceWater
          self.population = population
          self.residents = residents
          self.films = films
          self.created = created
          self.edited = edited
          self.url = url
      }
  }
