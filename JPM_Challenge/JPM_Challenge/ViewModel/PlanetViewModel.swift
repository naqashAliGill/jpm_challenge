//
//  PlanetViewModel.swift
//  JPM_Challenge
//
//  Created by Gill on 31/05/2023.
//

import Foundation
import UIKit
struct PlanetViewModel {
    
    private let _cdPlanetDataRepository : PlanetCoreDataRepository = PlanetsCoreDataRepo()
    private let _planetApiRepository: PlanetApiResourceRepository = PlanetsApiRepo()
    
    func getPlanetRecord(completionHandler:@escaping(_ result: Array<Results>?)-> Void) {
        if NetworkReachability.isConnectedToNetwork() {
         
                _planetApiRepository.getPlanetRecords { apiResponse in
                    if(apiResponse != nil && apiResponse?.count != 0){
                       
                        // Deleting Previously stored Records in CoreData
                        PersistentStorage.shared.resetAllRecords(in: "CD_Planet")
                        // Inserting Updated Data Into CoreData
                        _ = _cdPlanetDataRepository.insertPlanetRecords(records: apiResponse!)
                        debugPrint("PlanetViewModel: Returning records to view controller")
                        completionHandler(apiResponse)
                        
                    }
                }
        
        } else {
            
            // If Network is not Avalable Geting data From CoreData
            _cdPlanetDataRepository.getPlanetRecords { response in
                if(response != nil && response?.count != 0){
                    completionHandler(response)
                }else{
                    NotificationCenter.default.post(name: Notification.Name("StopActivityIndicator"), object: nil)
                    
                    print("Network Issue")
                }
            }
        }
        
        
        
        //
        //        _cdPlanetDataRepository.getPlanetRecords { response in
        //            if(response != nil && response?.count != 0){
        //
        //                // return response to the view controller
        //                completionHandler(response)
        //            }else {
        //                // call the api
        //                _planetApiRepository.getPlanetRecords { apiResponse in
        //                    if(apiResponse != nil && apiResponse?.count != 0){
        //                        // insert record in core data
        //                        _ =
        //                      _ = _cdPlanetDataRepository.insertPlanetRecords(records: apiResponse!)
        //                        debugPrint("PlanetViewModel: Returning records to view controller")
        //                        completionHandler(apiResponse)
        //                    }
        //                }
        //            }
        //        }
        //
    }
    
}
