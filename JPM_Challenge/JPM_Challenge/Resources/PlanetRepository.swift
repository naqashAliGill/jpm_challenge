//
//  PlanetRepository.swift
//  JPM_Challenge
//
//  Created by Gill on 30/05/2023.
//

import Foundation


protocol PlanetBaseRepository {
    
    func getPlanetRecords(completionHandler:@escaping(_ result: Array<Results>?)->Void)
}

protocol PlanetCoreDataRepository : PlanetBaseRepository {
    func insertPlanetRecords(records:Array<Results>) -> Bool
}

protocol PlanetApiResourceRepository : PlanetBaseRepository {
    
}

protocol PlanetRepository {
    func getPlanetRecords(completionHandler:@escaping(_ result: Array<Results>?)->Void)
}
