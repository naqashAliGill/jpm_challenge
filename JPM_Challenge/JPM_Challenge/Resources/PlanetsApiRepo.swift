//
//  PlanetsApiRepo.swift
//  JPM_Challenge
//
//  Created by Gill on 30/05/2023.
//

import Foundation

struct PlanetsApiRepo : PlanetApiResourceRepository {
    
    func getPlanetRecords(completionHandler: @escaping (Array<Results>?) -> Void) {
        
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: Constants.requestUrl) { (data, response, error) in
            //print(response)
            
            if  error != nil {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name("StopActivityIndicator"), object: nil)
                }
                print(String(describing: error))
                return
            }
            let httpResponse = response as? HTTPURLResponse
            if httpResponse?.statusCode != 200 {
                
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name("StopActivityIndicator"), object: nil)
                }
            }
            
            if let safeData = data  {
                
                print(String(data: safeData, encoding: .utf8)!)
                
                let decoder = JSONDecoder()
                let decodedData = try? decoder.decode(PlanetsModelList.self, from: safeData)
                
                debugPrint("decooooded>>>>", decodedData?.results)
                
                completionHandler(decodedData?.results) // sending decoded response back
                
            }
            
        }.resume()
    }
    
}

