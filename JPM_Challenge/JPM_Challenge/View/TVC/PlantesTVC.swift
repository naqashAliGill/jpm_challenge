//
//  PlantesTVCTableViewCell.swift
//  JPM_Challenge
//
//  Created by Gill on 31/05/2023.
//

import UIKit

class PlantesTVC: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDiameter: UILabel!
    @IBOutlet weak var lblPopuplation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
