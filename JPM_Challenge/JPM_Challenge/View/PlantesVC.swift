//
//  ViewController.swift
//  JPM_Challenge
//
//  Created by Gill on 30/05/2023.
//

import UIKit

class PlantesVC: UIViewController {
    
    // MARK: -  IB Outlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tbPlanets: UITableView!
    @IBOutlet weak var lblDataNotFound: UILabel!
    
    // MARK: -  Variables
    var planetsModel = PlanetsModelList()
    var planets : Array<Results>? = nil
    private let planetViewModel = PlanetViewModel()
    
    
    // MARK: -  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Planets"
        self.tbPlanets.delegate = self
        self.tbPlanets.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector:#selector(stopIndicator(_:)), name: Notification.Name("StopActivityIndicator"),object: nil)
        self.activityIndicator.startAnimating()
        
        planetViewModel.getPlanetRecord { [weak self] result in
            
            if(result != nil && result?.count != 0){
                self?.planets = result
                print(self?.planets?[0].name)
                
                DispatchQueue.main.async {
                    self?.tbPlanets.reloadData()
                    self?.activityIndicator.stopAnimating()
                }
                
                
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("StopActivityIndicator"), object: nil)
    }
    
    // MARK: - Notification Observer Function

    @objc func stopIndicator(_ notification: Notification) {
        activityIndicator.stopAnimating()
        let alert = UIAlertController(title: "Alert", message: "Something Went Wrong!", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.view.bringSubviewToFront(self.lblDataNotFound)
    }
    
    
    
    
}


