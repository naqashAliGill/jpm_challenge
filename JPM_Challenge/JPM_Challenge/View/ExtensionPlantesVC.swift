//
//  ExtensionPlantesVC.swift
//  JPM_Challenge
//
//  Created by Gill on 31/05/2023.
//

import Foundation
import UIKit

extension PlantesVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        planets?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlantesTVC") as! PlantesTVC

        let obj = self.planets?[indexPath.row]
        cell.lblName.text = obj?.name
        cell.lblDiameter.text = obj?.diameter

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PlanetDetailVC") as! PlanetDetailVC
        vc.planets = self.planets
        vc.indxNumber = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
