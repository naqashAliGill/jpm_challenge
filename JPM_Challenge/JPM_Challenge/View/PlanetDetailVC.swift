//
//  PlanetDetailVC.swift
//  JPM_Challenge
//
//  Created by Gill on 31/05/2023.
//

import UIKit

class PlanetDetailVC: UIViewController {

    // MARK: - IB Outlets

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDiameter: UILabel!
    @IBOutlet weak var lblClimate: UILabel!
    @IBOutlet weak var lblGravity: UILabel!
    @IBOutlet weak var lblSurface_water: UILabel!
    @IBOutlet weak var lblPopulation: UILabel!
    @IBOutlet weak var lblTerrain: UILabel!
    @IBOutlet weak var lblRotation_period: UILabel!
    @IBOutlet weak var lblOrbital_period: UILabel!
    
    // MARK: - Variables

    var planets : Array<Results>? = nil
    var indxNumber:Int?
    
    // MARK: - View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Planet's Detail"
        
        let  obj = self.planets?[indxNumber ?? 0]
        self.lblName.text = obj?.name
        self.lblDiameter.text = obj?.diameter
        self.lblClimate.text = obj?.climate
        self.lblGravity.text = obj?.gravity
        self.lblSurface_water.text = obj?.surface_water
        self.lblPopulation.text = obj?.population
        self.lblTerrain.text = obj?.terrain
        self.lblRotation_period.text = obj?.rotation_period
        self.lblOrbital_period.text = obj?.orbital_period
        
        
    }
    

    // MARK: - Functions

   
}
