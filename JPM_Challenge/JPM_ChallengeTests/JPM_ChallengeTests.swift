//
//  JPM_ChallengeTests.swift
//  JPM_ChallengeTests
//
//  Created by Gill on 30/05/2023.
//

import XCTest
@testable import JPM_Challenge

final class JPM_ChallengeTests: XCTestCase {
    let url =  Constants.requestUrl
    
    var apiRepo: PlanetsApiRepo!
    var coreDataRepo: PlanetsCoreDataRepo!
    override func setUp() {
        super.setUp()
        apiRepo = PlanetsApiRepo()
        coreDataRepo = PlanetsCoreDataRepo()
    }
    
    override func tearDown() {
        apiRepo = nil
        coreDataRepo = nil
        super.tearDown()
    }
    
    
    // Testing Model Lyer using mockdat to create a fake request respose
    func testSearchResponse() throws {
        
        
        guard let fileUrl = Bundle.main.url(forResource: "MockData", withExtension: "json")else {
            XCTFail("Missing file: MockData.json")
            return
        }
        
        let data = try Data(contentsOf:  fileUrl)
        let response = try JSONDecoder().decode(PlanetsModelList.self, from: data)
        
        
        XCTAssertEqual(response.count, 60)
        
        let repo = response.results?[0]
        
        XCTAssertEqual(repo?.name , "Tatooine")
        XCTAssertEqual(repo?.diameter, "10465")
        XCTAssertEqual(repo?.climate, "arid")
        XCTAssertEqual(repo?.gravity, "1 standard")
        XCTAssertEqual(repo?.terrain, "desert")
        XCTAssertEqual(repo?.surface_water, "1")
        XCTAssertEqual(repo?.population, "200000")
        XCTAssertEqual(repo?.rotation_period, "23")
        XCTAssertEqual(repo?.orbital_period, "304")
      
    }
    
    // This will Test the URL is changed or not
    func testValidUrl()
    {
        do {
            _ = try XCTUnwrap(url, "Invalid URL, url found nil")
        } catch { }
        XCTAssertEqual(url,URL(string:"https://swapi.dev/api/planets/"),"Planet list url should match the requirement")
    }
    
    // Testing Api Response working in GetPlanetRecord Function
    func testGetPlanetRecords() {
        let expectation = XCTestExpectation(description: "Get planet records")
        
        apiRepo.getPlanetRecords { (results) in
            XCTAssertNotNil(results, "Results should not be nil")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 20.0) // Adjust the timeout as needed
    }
    
    // Teesting The Records inserting in coredata
    func testInsertPlanetRecords() {
        let results = [Results(name: "Tatooine",
                               rotationPeriod: "23",
                               orbitalPeriod: "304",
                               diameter: "10465",
                               climate: "arid",
                               gravity: "1 standard",
                               terrain: "desert",
                               surfaceWater: "1",
                               population: "200000",
                               residents: [],
                               films: [],
                               created: "2014-12-09T13:50:49.641000Z",
                               edited: "2014-12-20T20:58:18.411000Z",
                               url: "https://swapi.dev/api/planets/1/")]
        
        let success = coreDataRepo.insertPlanetRecords(records: results)
        
        XCTAssertTrue(success, "Insertion should be successful")
    }
    
    
}
